from django.shortcuts import render
from matplotlib import pylab
from pylab import *
# import PIL
# import PIL.Image
import StringIO
from django.http import HttpResponse
from django.template import Context, loader
from models import Temperature
import datetime
# Create your views here.


def graph(request):
	# Temperature.objects.all().delete()
	# Temperature.objects.create(temp_C = 30, count = 0)
	temp_text = Temperature.objects.get(count=0)
	timenow = datetime.datetime.now()
	t = loader.get_template('tempSensor/index.html')
	c = Context({
        'temp_text': temp_text,
        'time': timenow
    })

	# temp2 = Temperature.objects.create(temp_C = 20, count = 1)
	# temp_text2 = Temperature.objects.get(count=1)
	return HttpResponse(t.render(c))
